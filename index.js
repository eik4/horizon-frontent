import storeModule from './store/index.js'

(async () => {
  // fetching config files
  const config = await DM.get('./config.json', 'json')

  // adding global styles
  const styles = []
  Object.entries(config.css).forEach(([name, path]) => {
    styles.push(new Promise((resolve) => {
      DM.get(path, 'text'). then((css) => {
        SSC.addStyleSheet(name, css)
          resolve()
      })
    }))
  })
  await Promise.all(styles)

  // injecting loaded config into Vue instances globally
  const plugin = {}
  plugin.install = function () {
    function getAddress(path = '') {
      let trimmedPath = path.trim()
      if (trimmedPath[0] !== '/') {
        trimmedPath = `/${trimmedPath}`
      }
      if (this.config.api.mode === 'dev') {
        return `http://${this.config.api.host}:${this.config.api.port}${trimmedPath}`
      } else {
        return trimmedPath
      }
    }

    const context = { config }

    Vue.prototype.$horizon = {
      config,
      getAddress: getAddress.bind(context)
    }
  }
  Vue.use(plugin)

  // injecting vuex
  Vue.use(Vuex)

  const store = new Vuex.Store(storeModule)

  // compiling the root component
  const components = await getVueComponents({ name: 'app', path: '/components/app' })

  if (components) {
    // initializing vue
    new Vue(getRoot(components, store))
  } else {
    console.error('Vue initialization error')
  }

})()