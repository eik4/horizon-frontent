const defaultState = {
  hotkeys: {}
}


export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    // hotkey: {
    //   keys: [String],
    //   name: String
    //   handler: String
    // }
    addHotkey(state, hotkey) {
      const copy = JSON.parse(JSON.stringify(hotkey))
      const params = {
        keys: copy.keys.sort().join('-'),
        handler: copy.handler
      }
      state.hotkeys[hotkey.name] = params
    },

    removeHotkey(state, hotkeyName) {
      delete state.hotkeys[hotkeyName]
    }
  }
}