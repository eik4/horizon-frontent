const defaultState = {
  source: null,
  points: null,
  maxCost: 2100,
  nearestRadius: 25,
  loading: false,
  result: {
    points: true,
    polygons: true
  }
}


export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    setSource(state, source) {
      state.source = source
    },

    setPoints(state, points) {
      state.points = points
    },

    // params: {
    //   coordinateIndex: Number,
    //   value: Number
    // }
    updateSource(state, params) {
      if (state.source) {
        state.source[params.coordinateIndex] = params.value
      }
    },

    removeSource(state) {
      state.source = null
    },

    setMaxCost(state, maxCost) {
      let mc = maxCost
      if (mc < 0) {
        mc = 0
      }
      state.maxCost = mc
    },

    setNearestRadius(state, nearestRadius) {
      let nr = nearestRadius
      if (nr < 0) {
        nr = 0
      }
      state.nearestRadius = nr
    },

    // params: {
    //   key: String,
    //   value: Any
    // }
    setResult(state, params) {
      if (state.result[params.key] !== undefined) {
        state.result[params.key] = params.value
      }
    },

    setLoading(state, loading) {
      state.loading = loading
    }
  },

  actions: {
    async get(vuex, vm) {
      if (vuex.state.source) {
        vuex.commit('setLoading', true)
        const response = await fetch(vm.$horizon.getAddress('/api/v0.1.0/isochrones'), {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({
            lonLat: vuex.state.source,
            maxCost: parseInt(vuex.state.maxCost),
            nearestRadius: parseInt(vuex.state.nearestRadius)
          })
        })
        const json = await response.json()
        vuex.commit('setPoints', json.data)
        vuex.commit('setLoading', false)
      }
    }
  },

  getters: {
    sourceGeojson(state) {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      if (state.source) {
        geojson.features.push({
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: state.source
          },
          properties: {
            color: '#000',
            opacity: 0.54,
            radius: 10,
            cursor: 'move'
          }
        })
      }
      return geojson
    },

    points(state) {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      if (state.points) {
        let min = null
        let max = null

        const rainbow = new Rainbow()
        rainbow.setSpectrum('#00cc00', '#cccc00', '#cc0000')

        state.points.features.forEach((feature) => {
          if (min === null || feature.properties.cost < min) {
            min = feature.properties.cost
          }
          if (max === null || feature.properties.cost > max) {
            max = feature.properties.cost
          }
        })

        state.points.features.forEach((feature) => {
          // const position = parseInt((((feature.properties.cost - min) / (max - min)) * 100).toFixed(0))
          const position = ((feature.properties.cost - min) / (max - min)) * 100
          const properties = {
            ...feature.properties,
            color: `#${rainbow.colourAt(position)}`,
            opacity: 0.54,
            radius: 5
          }
          geojson.features.push({
            ...feature,
            properties: properties
          })
        })
      }

      return geojson
    },

    polygons(state) {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      if (state.points) {
        let min = null
        let max = null

        const rainbow = new Rainbow()
        rainbow.setSpectrum('#00cc00', '#cccc00', '#cc0000')

        state.points.features.forEach((feature) => {
          if (min === null || feature.properties.cost < min) {
            min = feature.properties.cost
          }
          if (max === null || feature.properties.cost > max) {
            max = feature.properties.cost
          }
        })

        const iterations = 10
        const diff = max - min
        const step = diff / iterations

        const polygons = []
        let union = null
        let gap = min + step
        for (let i = 0; i < iterations; i++) {
          const geojsonCopy = JSON.parse(JSON.stringify(geojson))
          geojsonCopy.features = state.points.features.filter((feature) => {
            return feature.properties.cost <= gap
          })
          const polygon = turf.convex(geojsonCopy)
          polygon.properties.color = `#${rainbow.colourAt(i * 10)}`
          polygon.properties.opacity = 0.54

          let difference
          if (union === null) {
            difference = JSON.parse(JSON.stringify(polygon))
          } else {
            difference = turf.difference(polygon, union)
            if (difference === null) {
              difference = JSON.parse(JSON.stringify(polygon))
            }
          }

          polygons.push(difference)

          if (union === null) {
            union = JSON.parse(JSON.stringify(polygon))
          } else {
            union = turf.union(union, polygon)
          }

          gap = gap + step
        }
        geojson.features = polygons
      }

      return geojson
    },
  }
}