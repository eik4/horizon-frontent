const defaultState = {
  loaded: false,
  iconsLoaded: false,
  defaultCursor: '',
  clickHandler: '',
  mousedownHandler: '',
  mouseupHandler: '',
  mousemoveHandler: '',
  cursorPoint: null,
  layers: {
    mapMatchingPoints: 'horizon-map-matching-points',
    mapMatchingLine: 'horizon-map-matching-line',
    shortestPathPoints: 'horizon-shortest-path-points',
    shortestPathLine: 'horizon-shortest-path-line',
    isochronesSource: 'horizon-isochrones-source',
    isochronesPoints: 'horizon-isochrones-points',
    isochronesPolygons: 'horizon-isochrones-polygons'
  }
}


export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    setLoaded(state, loaded) {
      state.loaded = loaded
    },

    setIconsLoaded(state, loaded) {
      state.iconsLoaded = loaded
    },

    setDefaultCursor(state, cursor) {
      state.defaultCursor = cursor
    },

    setClickHandler(state, handlerName) {
      state.clickHandler = handlerName
    },

    setMousedownHandler(state, handlerName) {
      state.mousedownHandler = handlerName
    },

    setMouseupHandler(state, handlerName) {
      state.mouseupHandler = handlerName
    },

    setMousemoveHandler(state, handlerName) {
      state.mousemoveHandler = handlerName
    },

    setCursorPoint(state, point) {
      state.cursorPoint = point
    }
  }
}