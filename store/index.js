import mapbox from './mapbox.js'
import interfaceStore from './interface.js'
import mapMatching from './map-matching.js'
import shortestPath from './shortest-path.js'
import keyboard from './keyboard.js'
import isochrones from './isochrones.js'

export default {
  modules: {
    mapbox,
    interface: interfaceStore,
    mapMatching,
    shortestPath,
    keyboard,
    isochrones
  }
}