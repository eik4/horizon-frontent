const defaultState = {
  loaded: false,
  expanded: true,
  apis: {
    mapmatch: { name: 'Map Matching' },
    shortest: { name: ' Shortest Path' },
    isochrones: { name: 'Isochrones' }
  },
  currentAPI: null,
  width: 360
}


export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    setLoaded(state, loaded) {
      state.loaded = loaded
    },

    toggle(state) {
      state.expanded = !state.expanded
    },

    setCurrentAPI(state, api) {
      state.currentAPI = api
    }
  }
}