const defaultState = {
  points: [],
  selectedPoint: null,
  maxPoints: 16,
  maxStates: 5,
  stateRadius: 25,
  line: {
    type: 'FeatureCollection',
    features: []
  },
  loading: false
}


export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    addPoint(state, point) {
      state.points.push(point)
    },

    // params: {
    //   index: Number,
    //   coordinateIndex: Number,
    //   value: Number
    // }
    updatePoint(state, params) {
      if (state.points[params.index]) {
        if (state.points[params.index].lonLat[params.coordinateIndex] !== undefined) {
          state.points[params.index].lonLat[params.coordinateIndex] = params.value
        }
      }
    },

    removePoint(state, index) {
      state.points.splice(index, 1)
    },

    removeAllPoints(state) {
      while (state.points.length > 0) {
        state.points.splice(0, 1)
      }
    },

    setMaxStates(state, maxStates) {
      let ms = maxStates
      if (ms < 1) {
        ms = 1
      }
      if (ms > 10) {
        ms = 10
      }
      state.maxStates = ms
    },

    setStateRadius(state, stateRadius) {
      let sr = stateRadius
      if (sr < 7) {
        sr = 7
      }
      if (sr > 50) {
        sr = 50
      }
      state.stateRadius = stateRadius
    },

    setLine(state, line) {
      state.line = line
    },

    setLoading(state, loading) {
      state.loading = loading
    }
  },

  actions: {
    async match(vuex, vm) {
      if (vuex.state.points.length >= 3) {
        vuex.commit('setLoading', true)
        const response = await fetch(vm.$horizon.getAddress('/api/v0.1.0/mapmatch'), {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          body: JSON.stringify({
            gps: vuex.state.points,
            maxStates: parseInt(vuex.state.maxStates),
            stateRadius: parseInt(vuex.state.stateRadius)
          })
        })
        const json = await response.json()
        if (json) {
          vuex.commit('setLine', json.data)
        }
        vuex.commit('setLoading', false)
      }
    }
  },

  getters: {
    pointsGeojson(state) {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      state.points.forEach((point, index) => {
        geojson.features.push({
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: point.lonLat
          },
          properties: {
            icon: `map-matching-idle-${index + 1}`,
            index,
            cursor: 'move'
          }
        })
      })
      return geojson
    }
  }
}