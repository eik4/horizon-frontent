# horizon_frontend
-----

## Installation

There are no bundler so no extra dependencies necessary.

## Launching

1. Setup the Horizon according to official [instructions](https://github.com/LdDl/horizon)

2. Edit host and port on `config.json` in case you use custom ones.

3. Serve `index.html` with any web-server. `$ npx serve` will do the work well.