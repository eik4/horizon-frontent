// class for compiling Vue component from 3 files (html, js and css)
class VueComponent {

  // params contains files to be compiled
  constructor (name, path, params = { js: 'index.js' }) {
    // name of the component to be used on html
    this.name = name
    // the path of directory with coponent files
    this.path = path
    this.params = params
    this.js = null
    this.html = null
    this.css = null
    this.valid = true
  }

  // getting all necessary files
  fetch() {
    return new Promise((resolve) => {

      const promises = []

      // fetching html file if necessary
      if (this.params.html) {
        promises.push(new Promise((htmlResolve, htmlReject) => {
          DM.get(`${this.path}/${this.params.html}`, 'text')
            .then((htmlText) => {
              if (htmlText) {
                this.html = htmlText
                htmlResolve()
              } else {
                this.html = null
                htmlReject(new Error(`[${this.name}] no '${this.params.html}' on '${this.path}'`))
              }
            })
        }))
      }

      // fetching css file if necessary
      if (this.params.css) {
        promises.push(new Promise((cssResolve, cssReject) => {
          DM.get(`${this.path}/${this.params.css}`, 'text')
            .then((cssText) => {
              if (cssText) {
                this.css = cssText
                cssResolve()
              } else {
                this.css = null
                cssReject(new Error(`[${this.name}] no '${this.params.css}' on '${this.path}'`))
              }
            })
        }))
      }

      // importing module from js file
      if (this.params.js) {
        promises.push(new Promise((jsResolve, jsReject) => {
          DM.get(`${this.path}/${this.params.js}`, 'module')
            .then((module) => {
              if (module) {
                this.js = module
                jsResolve()
              } else {
                console.log(`Error while importing module '${this.path}/${this.params.js}'`)
                this.js = null
                jsReject(err)
              }
            })
        }))
      }

      // resolving when all fetchings and importing has been finished successfully
      Promise.all(promises)
        .then(() => {
          this.valid = true
          resolve(this)
        })
        .catch((err) => {
          this.valid = false
          console.error(err)
        })
    })
  }

  async compile() {
    await this.fetch()

    // adding stylesheets if necessary
    if (this.valid) {
      if (this.css) {
        SSC.addStyleSheet(this.path, this.css)
      }

      // calling module to get Vue component contents
      return Vue.component(this.name, await this.js({
        name: this.name,
        path: this.path,
        html: this.html
      }))
    } else {
      return null
    }
  }
}