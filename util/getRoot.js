// root vue component
function getRoot(components, store) {
  return {
    el: '#main',

    store,

    components,

    data() {
      return {
        pressedKeys: []
      }
    },

    beforeMount() {
      // base event bus methods
      this.$root.$on('eventBus-enable', this.enable)
      this.$root.$on('eventBus-disable', this.disable)
    },

    mounted() {
      document.onkeydown = (e) => {
        // getting pressed keys
        if (!this.pressedKeys.includes(e.code)) {
          this.pressedKeys.push(e.code)
        }

        // ignoring hotkey handlers if either input or button focused
        if ([
          'input',
          'button'
        ].includes(e.target.nodeName.toLowerCase())) {
          return
        }

        // making hotkey id
        const keys = JSON.parse(JSON.stringify(this.pressedKeys)).sort().join('-')

        // calling hotkey handlers if present
        const match = Object.values(this.$store.state.keyboard.hotkeys).filter((hotkey) => {
          return hotkey.keys === keys
        })
        match.forEach((hotkey) => {
          this.$root.$emit(hotkey.handler)
        })
      }

      document.onkeyup = (e) => {
        // removing pressed keys
        this.pressedKeys = this.pressedKeys.filter((key) => { return key !== e.code })
      }

      // setting interface loaded flag to true
      this.$store.commit('interface/setLoaded', true)
    },

    methods: {
      // bulk enabling events
      enable(obj) {
        for (const [name, handler] of Object.entries(obj)) {
          this.$root.$on(name, handler)
        }
      },

      // bulk disabling events
      disable(obj) {
        for (const [name] of Object.entries(obj)) {
          this.$root.$off(name)
        }
      }
    },

    beforeDestroy() {
      this.$root.$off('eventBus-enable')
      this.$root.$off('eventBus-disable')
    }
  }
}