// making the Vue component of givent files
// can be either an object of VueComponent params or an array of the objects
function getVueComponents(params) {
  const components = {}

  if (Array.isArray(params)) {
    const promises = []
    params.forEach((param) => {
      promises.push(
        new Promise((resolve) => {
          (new VueComponent(param.name, param.path, { html: `${param.name}.html`, js: `${param.name}.js`, css: `${param.name}.css` })).compile()
            .then((component) => {
              components[param.name] = component
              resolve()
            })
        })
      )
    })

    return new Promise((resolve) => {
      Promise.all(promises).then(() => {
        resolve(components)
      })
    })
  } else if (typeof params === 'object') {
    return new Promise((resolve) => {
      (new VueComponent(params.name, params.path, { html: `${params.name}.html`, js: `${params.name}.js`, css: `${params.name}.css` })).compile()
          .then((component) => {
            components[params.name] = component
            resolve(component)
          })
    })
  }
}