// dynamically apllying stylesheets
// depending on loaded components
class StyleSheetController {

  constructor() {
    this.styleSheets = {}
  }

  addStyleSheet(name, rules) {
    if (this.styleSheets[name] === undefined) {
      this.styleSheets[name] = { dom: document.createElement('style'), loaded: true }
      this.styleSheets[name].dom.innerHTML = rules
      this.styleSheets[name].dom.dataset.name = name
      document.body.appendChild(this.styleSheets[name].dom)
    }
  }

  updateStyleSheet(name, rules) {
    if (this.styleSheets[name] !== undefined) {
      this.styleSheets[name].dom.remove()
      this.styleSheets[name].dom.innerHTML = rules
      document.body.appendChild(this.styleSheets[name].dom)
    }
  }

  unloadStyleSheet(name) {
    if (this.styleSheets[name] !== undefined) {
      if (this.styleSheets[name].loaded) {
        this.styleSheets[name].dom.remove()
        this.styleSheets[name].loaded = false
      }
    }
  }

  loadStyleSheet(name) {
    if (this.styleSheets[name] !== undefined) {
      if (!this.styleSheets[name].loaded) {
        document.body.appendChild(this.styleSheets[name].dom)
        this.styleSheets[name].loaded = true
      }
    }
  }

  removeStyleSheet(name) {
    if (this.styleSheets[name] !== undefined) {
      if (this.styleSheets[name].loaded) {
        this.styleSheets[name].dom.remove()
      }
      delete this.styleSheets[name]
    }
  }

}

const SSC = new StyleSheetController()