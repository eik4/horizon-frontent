// DynamicModules implements "hard-caching" by storing
// fetched files or imported modules on single object

class DynamicModules {
  constructor() {
    // fetched files or imported modules
    // key is relative path to the file
    // value is content of the file or the module
    this.files = {}
  }

  // get the file content
  // mode is either 'js' or any Response method (https://developer.mozilla.org/en-US/docs/Web/API/Response)
  // params can contain { force: true } to force fetching of existing file
  get(path, mode, params = {}) {

    // removing present file if necessary
    if (params.force) {
      this.remove(path)
    }

    // if the file is absent
    if (this.files[path] === undefined) {
      const promise = new Promise((resolve) => {
        if (mode === 'module') {
          // resolving module's default
          import(path)
            .then((module) => {
              this.files[path] = module.default
              resolve(module.default)
            })
            .catch(() => {
              resolve(null)
            })
        } else {
          // resolving given method for everything else
          fetch(path).then((response) => {
            response[mode]().then((text) => {
              this.files[path] = text
              resolve(text)
            })
          })
        }
      })

      // saving the promise immediately in order to
      // prevent multiple queries at the same time
      this.files[path] = promise
      return promise
    // if the file is present
    } else {
      return new Promise((resolve) => {
        resolve(this.files[path])
      })
    }
  }

  remove(path) {
    if (this.files[path] !== undefined) {
      delete this.files[path]
    }
  }
}

const DM = new DynamicModules()