export default function (params) {
  return new Promise(async (resolve) => {
    resolve({
      name: params.name,

      template: params.html,

      data() {
        return {
          svg: null
        }
      },

      props: {
        name: {
          type: String,
          required: true
        },

        width: {
          type: Number,
          required: true
        },

        height: {
          type: Number,
          required: true
        },
      },

      created() {
        // fetching icon by its name from specific directory
        DM.get(`/components/ui/icon/svg/${this.name}.svg`, 'text')
          .then((text) => {
            const parser = new DOMParser()
            const doc = parser.parseFromString(text, "text/html")
            const svg = doc.querySelector('svg').cloneNode(true)
            if (this.$el) {
              this.$el.appendChild(svg)
            } else {
              this.svg = svg
            }
          })
      },

      mounted() {
        if (this.svg) {
          this.$el.appendChild(this.svg)
        }
      },

      beforeDestroy() {
        this.$root.$emit('ssc-removeStyleSheet', params.path)
      }
    })
  })
}