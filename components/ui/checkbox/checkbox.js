export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents(
      { name: 'icon', path: '/components/ui/icon' }
    ).then((components) => {

      resolve({
        name: params.name,

        template: params.html,

        components,

        data() {
          return { }
        },

        props: {
          modelVal: {
            type: Boolean
          },

          label: {
            type: String,
            default: null
          },
          
          value: {
            type: Boolean,
            required: true
          }
        },

        model: {
          prop: 'modelVal',
          event: 'mutateModelVal'
        },

        computed: {
          modelValLocal: {
            get: function() {
              return this.modelVal
            },
            set: function(value) {
              this.$emit('mutateModelVal', value)
              if (value) {
                this.$emit('onenable')
              } else {
                this.$emit('ondisable')
              }
            }
          }
        },

        methods: {
          change() {
            this.modelValLocal = !this.modelValLocal
          },
        },

        beforeDestroy() {
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        }
      })
    })
  })
}