export default function (params) {
  return new Promise(async (resolve) => {
    resolve({
      name: params.name,

      template: params.html,

      data() {
        return {
          // width and height of the icon
          size: 100,

          // id for breaking the animation
          timerID: null
        }
      },

      mounted() {
        // defining maximum sizes of the icon
        let size = this.$el.offsetWidth
        if (size < this.$el.offsetHeight) {
          size = this.$el.offsetHeight
        }
        this.size = size / 10

        if (this.size > 100) {
          this.size = 100
        }

        // applying different opacities for each circle
        const opacity = [0.45, 0.40, 0.35, 0.30, 0.25, 0.20, 0.15, 0.1]
        this.timerID = setInterval(() => {
          opacity.forEach((o, index) => {
            this.$el.children[0].children[index].setAttribute('fill-opacity', o)
          })

          const curr = opacity.shift()
          opacity.push(curr)
        }, 100)
      },

      beforeDestroy() {
        this.$root.$emit('ssc-removeStyleSheet', params.path)
        if (this.timerID) {
          // stopping current animation
          clearInterval(this.timerID)
          this.timerID = null
        }
      }
    })
  })
}