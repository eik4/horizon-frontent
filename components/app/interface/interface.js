export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents([
      { name: 'main-control', path: '/components/app/interface/main-control' },
      { name: 'api-control', path: '/components/app/interface/api-control' },
    ]).then((components) => {
      resolve({
        name: params.name,

        template: params.html,

        components,

        computed: {
          expanded() {
            return this.$store.state.interface.expanded
          }
        },

        beforeDestroy() {
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        }
      })
    })
  })
}