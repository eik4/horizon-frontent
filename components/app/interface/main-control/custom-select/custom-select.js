export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents([
      { name: 'icon', path: '/components/ui/icon' }
    ]).then((components) => {
      resolve({
        name: params.name,

        template: params.html,

        components,

        data() {
          return {
            opened: false
          }
        },

        props: {
          placeholder: {
            type: String,
            default: 'placeholder'
          }
        },

        computed: {
          // options
          apis() {
            return this.$store.state.interface.apis
          },

          currentAPI() {
            return this.$store.state.interface.currentAPI
          },

          currentText() {
            if (this.currentAPI === null) {
              return null
            } else {
              if (this.apis[this.currentAPI]) {
                return this.apis[this.currentAPI].name
              } else {
                return null
              }
            }
          }
        },

        methods: {
          toggle() {
            this.opened = !this.opened
            this.$nextTick(() => {
              if (this.opened) {
                this.$el.style.height = `${this.$el.scrollHeight}px`
              } else {
                this.$el.style.height = '48px'
              }
            })
          },

          selectAPI(api) {
            this.$store.commit('interface/setCurrentAPI', api)
            this.toggle()
          }
        },

        beforeDestroy() {
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        }
      })
    })
  })
}