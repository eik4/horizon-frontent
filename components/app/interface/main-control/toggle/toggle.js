export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents([
      { name: 'icon', path: '/components/ui/icon' }
    ]).then((components) => {
      resolve({
        name: params.name,

        template: params.html,

        components,

        computed: {
          expanded() {
            return this.$store.state.interface.expanded
          }
        },

        methods: {
          toggle() {
            this.$store.commit('interface/toggle')
          }
        },

        beforeDestroy() {
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        }
      })
    })
  })
}