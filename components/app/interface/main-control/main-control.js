export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents([
      { name: 'custom-select', path: '/components/app/interface/main-control/custom-select' },
      { name: 'toggle', path: '/components/app/interface/main-control/toggle' }
    ]).then((components) => {
      resolve({
        name: params.name,

        template: params.html,

        components,

        beforeDestroy() {
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        }
      })
    })
  })
}