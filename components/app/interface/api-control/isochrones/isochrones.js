export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents([
      { name: 'icon', path: '/components/ui/icon' },
      { name: 'loading', path: '/components/ui/loading' },
      { name: 'checkbox', path: '/components/ui/checkbox' }
    ]).then((components) => {

      resolve({
        name: params.name,

        template: params.html,

        components,

        data() {
          return {
            maxCost: 2100,
            nearestRadius: 25,
            eventBusHandlers: {
              'isochrones-updateInputValues': this.updateInputValues,
              'isochrones-updateInputByIndex': this.updateInputByIndex,
              'isochrones-removeSource': this.removeSource,
              'isochrones-get': this.match,
              'isochrones-clear': this.clear
            },
            result: {
              points: true,
              polygons: true
            }
          }
        },

        computed: {
          source() {
            return this.$store.state.isochrones.source
          },

          points() {
            return this.$store.state.isochrones.points
          },

          loading() {
            return this.$store.state.isochrones.loading
          }
        },

        created() {
          this.result.points = this.$store.state.isochrones.result.points
          this.result.polygons = this.$store.state.isochrones.result.polygons
        },

        beforeMount() {
          this.$root.$emit('eventBus-enable', this.eventBusHandlers)
        },

        mounted() {
          this.$store.commit('mapbox/setDefaultCursor', 'crosshair')
          this.$store.commit('mapbox/setClickHandler', 'addIsochronesSource')
          this.$store.commit('mapbox/setMousedownHandler', 'dragstartIsochronesSource')
          this.$store.commit('mapbox/setMouseupHandler', 'dragendIsochronesSource')
          this.$store.commit('mapbox/setMousemoveHandler', 'dragIsochronesSource')

          if (this.source) {
            this.$root.$emit('mapbox-updateIsochronesSource')
            this.updateInputValues()
          }

          if (this.points) {
            this.$root.$emit('mapbox-updateIsochronesPoints')
            this.$root.$emit('mapbox-updateIsochronesPolygons')
          }

          this.stateRadius = this.$store.state.isochrones.stateRadius

          this.$store.commit('keyboard/addHotkey', {
            keys: ['Delete'],
            name: 'isochrones-remove-source-delete',
            handler: 'isochrones-removeSource'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Backspace'],
            name: 'isochrones-remove-source-backspace',
            handler: 'isochrones-removeSource'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Enter'],
            name: 'isochrones-get',
            handler: 'isochrones-get'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Escape'],
            name: 'isochrones-clear',
            handler: 'isochrones-clear'
          })
        },

        methods: {
          updateInput(dom, float) {
            const str = float.toString()
            let step = '1'
            if (str.includes('.')) {
              const fractional = str.split('.')[1].length
              for (let i = 0; i < fractional; i++) {
                if (step.length < 5) {
                  step = `0${step}`
                }
              }
            }
            if (step[0] === '0') {
              step = `0.${step}`
            }

            dom.setAttribute('step', step)

            dom.value = float
          },

          updateInputByIndex(index, lngLat) {
            const domPoints = document.querySelectorAll('.isochrones .isochrones__gps-point')
            this.updateInput(domPoints[index].children[1].children[0], lngLat.lng)
            this.updateInput(domPoints[index].children[2].children[0], lngLat.lat)
          },

          updateInputValues() {
            this.$nextTick(() => {
              const domPoints = document.querySelectorAll('.isochrones .isochrones__gps-point')
              if (this.source) {
                this.updateInput(domPoints[0].children[1].children[0], this.source[0])
                this.updateInput(domPoints[0].children[2].children[0], this.source[1])
              } else {
                domPoints[0].children[1].children[0].value = ''
                domPoints[0].children[2].children[0].value = ''
              }
            })
          },

          updatePoint(coordinateIndex, e) {
            this.$store.commit('isochrones/updateSource', {
              coordinateIndex,
              value: parseFloat(e.target.value)
            })
            this.$root.$emit('mapbox-updateIsochronesSource')
          },

          removePoint() {
            this.$store.commit('isochrones/removeSource')
            this.$root.$emit('mapbox-updateIsochronesSource')
            this.updateInputValues()
          },

          removeSource() {
            this.removePoint()
          },

          async get() {
            if (this.$store.state.isochrones.source) {
              await this.$store.dispatch('isochrones/get', this)
              this.$root.$emit('mapbox-updateIsochronesPoints')
              this.$root.$emit('mapbox-updateIsochronesPolygons')
            }
          },

          clear() {
            this.$store.commit('isochrones/removeSource')
            this.$store.commit('isochrones/setPoints', null)
            this.$root.$emit('mapbox-updateIsochronesSource')
            this.$root.$emit('mapbox-updateIsochronesPoints')
            this.$root.$emit('mapbox-updateIsochronesPolygons')
            this.maxCost = 2100
            this.nearestRadius = 25
          }
        },

        beforeDestroy() {
          this.$store.commit('mapbox/setDefaultCursor', '')
          this.$store.commit('mapbox/setClickHandler', '')
          this.$store.commit('mapbox/setMousedownHandler', '')
          this.$store.commit('mapbox/setMouseupHandler', '')
          this.$store.commit('mapbox/setMousemoveHandler', '')

          this.$store.commit('keyboard/removeHotkey', 'isochrones-remove-source-delete')
          this.$store.commit('keyboard/removeHotkey', 'isochrones-remove-source-backspace')
          this.$store.commit('keyboard/removeHotkey', 'isochrones-get')
          this.$store.commit('keyboard/removeHotkey', 'isochrones-clear')

          this.$root.$emit('mapbox-emptyIsochronesGeometries')
          this.$root.$emit('eventBus-disable', this.eventBusHandlers)
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        },

        watch: {
          source: function () {
            this.updateInputValues()
          },

          maxCost: function () {
            this.$store.commit('isochrones/setMaxCost', this.maxCost)
          },

          nearestRadius: function () {
            this.$store.commit('isochrones/setNearestRadius', this.nearestRadius)
          },

          'result.points': function () {
            this.$store.commit('isochrones/setResult', {
              key: 'points',
              value: this.result.points
            })
            if (this.result.points) {
              this.$root.$emit('mapbox-updateIsochronesPoints')
            } else {
              this.$root.$emit('mapbox-emptyIsochronesPoints')
            }
          },

          'result.polygons': function (n) {
            this.$store.commit('isochrones/setResult', {
              key: 'polygons',
              value: this.result.polygons
            })
            if (this.result.polygons) {
              this.$root.$emit('mapbox-updateIsochronesPolygons')
            } else {
              this.$root.$emit('mapbox-emptyIsochronesPolygons')
            }
          }
        }
      })
    })
  })
}