export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents(
      { name: 'icon', path: '/components/ui/icon' },
      { name: 'loading', path: '/components/ui/loading' },
    ).then((components) => {

      resolve({
        name: params.name,

        template: params.html,

        components,

        data() {
          return {
            stateRadius: 25,
            eventBusHandlers: {
              'shortest-path-updateInputByIndex': this.updateInputByIndex,
              'shortest-path-removeLastSPPoint': this.removeLastSPPoint,
              'shortest-path-match': this.match,
              'shortest-path-clear': this.clear
            }
          }
        },

        computed: {
          points() {
            return this.$store.state.shortestPath.points
          },

          additionalPoints() {
            return this.points.slice(3)
          },

          loading() {
            return this.$store.state.shortestPath.loading
          }
        },

        beforeMount() {
          this.$root.$emit('eventBus-enable', this.eventBusHandlers)
        },

        mounted() {
          this.$store.commit('mapbox/setDefaultCursor', 'crosshair')
          this.$store.commit('mapbox/setClickHandler', 'addNewSPPoint')
          this.$store.commit('mapbox/setMousedownHandler', 'dragstartSPPoint')
          this.$store.commit('mapbox/setMouseupHandler', 'dragendSPPoint')
          this.$store.commit('mapbox/setMousemoveHandler', 'dragSPPoint')

          if (this.points.length > 0) {
            this.$root.$emit('mapbox-updateSPPoints')
            this.$root.$emit('mapbox-updateShortestPathLine')
            this.updateInputValues()
          }

          this.stateRadius = this.$store.state.shortestPath.stateRadius

          this.$store.commit('keyboard/addHotkey', {
            keys: ['Delete'],
            name: 'shortest-path-remove-last-gps-point-delete',
            handler: 'shortest-path-removeLastSPPoint'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Backspace'],
            name: 'shortest-path-remove-last-gps-point-backspace',
            handler: 'shortest-path-removeLastSPPoint'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Enter'],
            name: 'shortest-path-match',
            handler: 'shortest-path-match'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Escape'],
            name: 'shortest-path-clear',
            handler: 'shortest-path-clear'
          })
        },

        methods: {
          updateInput(dom, float) {
            const str = float.toString()
            let step = '1'
            if (str.includes('.')) {
              const fractional = str.split('.')[1].length
              for (let i = 0; i < fractional; i++) {
                if (step.length < 5) {
                  step = `0${step}`
                }
              }
            }
            if (step[0] === '0') {
              step = `0.${step}`
            }

            dom.setAttribute('step', step)

            dom.value = float
          },

          updateInputByIndex(index, lngLat) {
            const domPoints = document.querySelectorAll('.shortest-path .shortest-path__gps-point')
            this.updateInput(domPoints[index].children[1].children[0], lngLat.lng)
            this.updateInput(domPoints[index].children[2].children[0], lngLat.lat)
          },

          updateInputValues() {
            this.$nextTick(() => {
              const domPoints = document.querySelectorAll('.shortest-path .shortest-path__gps-point')
              this.points.forEach((point, index) => {
                this.updateInput(domPoints[index].children[1].children[0], point.lonLat[0])
                this.updateInput(domPoints[index].children[2].children[0], point.lonLat[1])
              })
              if (this.points.length < domPoints.length) {
                for (let i = this.points.length; i < domPoints.length; i++) {
                  domPoints[i].children[1].children[0].value = ''
                  domPoints[i].children[2].children[0].value = ''
                }
              }
            })
          },

          updatePoint(index, coordinateIndex, e) {
            this.$store.commit('shortestPath/updatePoint', {
              index,
              coordinateIndex,
              value: parseFloat(e.target.value)
            })
            this.$root.$emit('mapbox-updateSPPoints')
          },

          removePoint(index) {
            this.$store.commit('shortestPath/removePoint', index)
            this.$root.$emit('mapbox-updateSPPoints')
          },

          removeLastSPPoint() {
            if (this.$store.state.shortestPath.points.length > 0) {
              this.removePoint(this.$store.state.shortestPath.points.length - 1)
            }
          },

          async match() {
            if (this.$store.state.shortestPath.points.length === 2) {
              await this.$store.dispatch('shortestPath/match', this)
              this.$root.$emit('mapbox-updateShortestPathLine')
            }
          },

          clear() {
            this.$store.commit('shortestPath/removeAllPoints')
            this.$root.$emit('mapbox-updateSPPoints')
            this.$store.commit('shortestPath/setLine', {
              type: 'FeatureCollection',
              features: []
            })
            this.$root.$emit('mapbox-updateShortestPathLine')
            this.stateRadius = 25
          }
        },

        beforeDestroy() {
          this.$store.commit('mapbox/setDefaultCursor', '')
          this.$store.commit('mapbox/setClickHandler', '')
          this.$store.commit('mapbox/setMousedownHandler', '')
          this.$store.commit('mapbox/setMouseupHandler', '')
          this.$store.commit('mapbox/setMousemoveHandler', '')

          this.$store.commit('keyboard/removeHotkey', 'shortest-path-remove-last-gps-point-delete')
          this.$store.commit('keyboard/removeHotkey', 'shortest-path-remove-last-gps-point-backspace')
          this.$store.commit('keyboard/removeHotkey', 'shortest-path-match')
          this.$store.commit('keyboard/removeHotkey', 'shortest-path-clear')

          this.$root.$emit('mapbox-emptyShortestPathGeometries')
          this.$root.$emit('eventBus-disable', this.eventBusHandlers)
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        },

        watch: {
          points: function () {
            this.updateInputValues()
          },

          stateRadius: function () {
            this.$store.commit('shortestPath/setStateRadius', this.stateRadius)
          }
        }
      })
    })
  })
}