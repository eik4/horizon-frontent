export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents(
      { name: 'icon', path: '/components/ui/icon' },
      { name: 'loading', path: '/components/ui/loading' },
    ).then((components) => {

      resolve({
        name: params.name,

        template: params.html,

        components,

        data() {
          return {
            maxStates: 5,
            stateRadius: 25,
            eventBusHandlers: {
              'map-matching-updateInputByIndex': this.updateInputByIndex,
              'map-matching-removeLastGpsPoint': this.removeLastGpsPoint,
              'map-matching-match': this.match,
              'map-matching-clear': this.clear
            }
          }
        },

        computed: {
          points() {
            return this.$store.state.mapMatching.points
          },

          additionalPoints() {
            return this.points.slice(3)
          },

          loading() {
            return this.$store.state.mapMatching.loading
          }
        },

        beforeMount() {
          this.$root.$emit('eventBus-enable', this.eventBusHandlers)
        },

        mounted() {
          this.$store.commit('mapbox/setDefaultCursor', 'crosshair')
          this.$store.commit('mapbox/setClickHandler', 'addNewGpsPoint')
          this.$store.commit('mapbox/setMousedownHandler', 'dragstartGpsPoint')
          this.$store.commit('mapbox/setMouseupHandler', 'dragendGpsPoint')
          this.$store.commit('mapbox/setMousemoveHandler', 'dragGpsPoint')

          if (this.points.length > 0) {
            this.$root.$emit('mapbox-updateGpsPoints')
            this.$root.$emit('mapbox-updateMapMatchingLine')
            this.updateInputValues()
          }

          this.maxStates = this.$store.state.mapMatching.maxStates
          this.stateRadius = this.$store.state.mapMatching.stateRadius

          this.$store.commit('keyboard/addHotkey', {
            keys: ['Delete'],
            name: 'map-matching-remove-last-gps-point-delete',
            handler: 'map-matching-removeLastGpsPoint'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Backspace'],
            name: 'map-matching-remove-last-gps-point-backspace',
            handler: 'map-matching-removeLastGpsPoint'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Enter'],
            name: 'map-matching-match',
            handler: 'map-matching-match'
          })
          this.$store.commit('keyboard/addHotkey', {
            keys: ['Escape'],
            name: 'map-matching-clear',
            handler: 'map-matching-clear'
          })
        },

        methods: {
          updateInput(dom, float) {
            const str = float.toString()
            let step = '1'
            if (str.includes('.')) {
              const fractional = str.split('.')[1].length
              for (let i = 0; i < fractional; i++) {
                if (step.length < 5) {
                  step = `0${step}`
                }
              }
            }
            if (step[0] === '0') {
              step = `0.${step}`
            }

            dom.setAttribute('step', step)

            dom.value = float
          },

          updateInputByIndex(index, lngLat) {
            const domPoints = document.querySelectorAll('.map-matching .map-matching__gps-point')
            this.updateInput(domPoints[index].children[1].children[0], lngLat.lng)
            this.updateInput(domPoints[index].children[2].children[0], lngLat.lat)
          },

          updateInputValues() {
            this.$nextTick(() => {
              const domPoints = document.querySelectorAll('.map-matching .map-matching__gps-point')
              this.points.forEach((point, index) => {
                this.updateInput(domPoints[index].children[1].children[0], point.lonLat[0])
                this.updateInput(domPoints[index].children[2].children[0], point.lonLat[1])
              })
              if (this.points.length < domPoints.length) {
                for (let i = this.points.length; i < domPoints.length; i++) {
                  domPoints[i].children[1].children[0].value = ''
                  domPoints[i].children[2].children[0].value = ''
                }
              }
            })
          },

          updatePoint(index, coordinateIndex, e) {
            this.$store.commit('mapMatching/updatePoint', {
              index,
              coordinateIndex,
              value: parseFloat(e.target.value)
            })
            this.$root.$emit('mapbox-updateGpsPoints')
          },

          removePoint(index) {
            this.$store.commit('mapMatching/removePoint', index)
            this.$root.$emit('mapbox-updateGpsPoints')
          },

          removeLastGpsPoint() {
            if (this.$store.state.mapMatching.points.length > 0) {
              this.removePoint(this.$store.state.mapMatching.points.length - 1)
            }
          },

          async match() {
            if (this.$store.state.mapMatching.points.length >= 2) {
              await this.$store.dispatch('mapMatching/match', this)
              this.$root.$emit('mapbox-updateMapMatchingLine')
            }
          },

          clear() {
            this.$store.commit('mapMatching/removeAllPoints')
            this.$root.$emit('mapbox-updateGpsPoints')
            this.$store.commit('mapMatching/setLine', {
              type: 'FeatureCollection',
              features: []
            })
            this.$root.$emit('mapbox-updateMapMatchingLine')
            this.maxStates = 5
            this.stateRadius = 25
          }
        },

        beforeDestroy() {
          this.$store.commit('mapbox/setDefaultCursor', '')
          this.$store.commit('mapbox/setClickHandler', '')
          this.$store.commit('mapbox/setMousedownHandler', '')
          this.$store.commit('mapbox/setMouseupHandler', '')
          this.$store.commit('mapbox/setMousemoveHandler', '')

          this.$store.commit('keyboard/removeHotkey', 'map-matching-remove-last-gps-point-delete')
          this.$store.commit('keyboard/removeHotkey', 'map-matching-remove-last-gps-point-backspace')
          this.$store.commit('keyboard/removeHotkey', 'map-matching-match')
          this.$store.commit('keyboard/removeHotkey', 'map-matching-clear')

          this.$root.$emit('mapbox-emptyMapMatchingGeometries')
          this.$root.$emit('eventBus-disable', this.eventBusHandlers)
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        },

        watch: {
          points: function () {
            this.updateInputValues()
          },

          maxStates: function () {
            this.$store.commit('mapMatching/setMaxStates', this.maxStates)
          },

          stateRadius: function () {
            this.$store.commit('mapMatching/setStateRadius', this.stateRadius)
          }
        }
      })
    })
  })
}