export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents([
        { name: 'map-matching', path: '/components/app/interface/api-control/map-matching' },
        { name: 'shortest-path', path: '/components/app/interface/api-control/shortest-path' },
        { name: 'isochrones', path: '/components/app/interface/api-control/isochrones' }
    ]).then((components) => {
      resolve({
        name: params.name,

        template: params.html,

        components,

        computed: {
          currentAPI() {
            return this.$store.state.interface.currentAPI
          }
        },

        beforeDestroy() {
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        }
      })
    })
  })
}