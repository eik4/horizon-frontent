export default function (params) {
  return new Promise(async (resolve) => {

    getVueComponents([
      { name: 'mapbox', path: '/components/app/mapbox' },
      { name: 'interface', path: '/components/app/interface' },
      { name: 'loading', path: '/components/ui/loading' }
    ]).then((components) => {
      resolve({
        name: params.name,
  
        template: params.html,
  
        components,

        computed: {
          // application is ready when both mapbox has loaded all styles/icons and interface has rendered
          appReady() {
            return this.$store.state.mapbox.loaded && this.$store.state.mapbox.iconsLoaded && this.$store.state.interface.loaded
          }
        },
  
        beforeDestroy() {
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        }
      })
    })
  })
}