export default function (params) {
  return new Promise(async (resolve) => {

    // importing mixins
    const mixins = []
    const mixinFiles = {
      'map-matching': 'map-matching.js',
      'shortest-path': 'shortest-path.js',
      'isochrones': 'isochrones.js'
    }
    const mixinPromises = []

    Object.entries(mixinFiles).forEach(([key, file]) => {
      mixinPromises.push(new Promise((mixinResolve, mixinReject) => {
        DM.get(`${params.path}/mixins/${file}`, 'module')
            .then((module) => {
              if (module) {
                mixins.push(module)
                mixinResolve()
              } else {
                mixinReject(`Error while fetching '${params.path}/mixins/${file}'`)
              }
            })
      }))
    })

    Promise.all(mixinPromises).then(() => {
      resolve({
        name: params.name,
  
        template: params.html,

        mixins,
  
        data() {
          return {
            map: null,

            // mouse click radius
            mousePadding: 3,
            eventBusHandlers: { },

            // layers to be added on map load
            addLayerHandlers: []
          }
        },
  
        computed: {
          defaultCursor() {
            return this.$store.state.mapbox.defaultCursor
          }
        },
  
        beforeMount() {
          this.$root.$emit('eventBus-enable', this.eventBusHandlers)
        },
  
        mounted() {
          // initializing mapbox
          mapboxgl.accessToken = 'pk.eyJ1IjoiZGltYWhraWluIiwiYSI6ImNqZmNqYWV3bjJxM2IzNG52M3cwNG9sbTEifQ.hBZWN6asfRuTVSKV6Ut1Bw'
          this.map = new mapboxgl.Map({
            container: "map",
            style: "mapbox://styles/dimahkiin/ck7q21t6z0ny71imt9v5valra",
            center: this.$horizon.config.mapbox.center,
            zoom: this.$horizon.config.mapbox.zoom,
            antialias: true
          })
  
          this.loadIcons()
  
          this.map.on('load', () => {
            this.addLayers()
            this.$store.commit('mapbox/setLoaded', true)
          })
  
          this.map.on('click', (e) => {
            if (this.$store.state.mapbox.clickHandler) {
              if (this[this.$store.state.mapbox.clickHandler]) {
                this[this.$store.state.mapbox.clickHandler](e)
              }
            }
          })
  
          this.map.on('mousedown', (e) => {
            if (this.$store.state.mapbox.mousedownHandler) {
              if (this[this.$store.state.mapbox.mousedownHandler]) {
                this[this.$store.state.mapbox.mousedownHandler](e)
              }
            }
          })
  
          this.map.on('mouseup', (e) => {
            if (this.$store.state.mapbox.mouseupHandler) {
              if (this[this.$store.state.mapbox.mouseupHandler]) {
                this[this.$store.state.mapbox.mouseupHandler](e)
              }
            }
          })
  
          this.map.on('mousemove', (e) => {
            this.$store.commit('mapbox/setCursorPoint', e.point)
            if (this.$store.state.mapbox.mousemoveHandler) {
              if (this[this.$store.state.mapbox.mousemoveHandler]) {
                this[this.$store.state.mapbox.mousemoveHandler](e)
              }
            }
            this.updateCursor()
          })
        },
  
        methods: {
          getFirstSymbolLayer() {
            const layers = this.map.getStyle().layers
            if (layers.length === 0) {
              return null
            }
            const symbolLayers = layers.filter((layer) => { return layer.type === 'symbol' })
            if (symbolLayers.length === 0) {
              return null
            }
            return symbolLayers[0].id
          },
  
          loadIcons() {
            const svgPromises = []
            const markers = [
              { file: '/assets/svg/marker.svg', prefix: 'map-matching-idle' }
              // { file: '/assets/svg/marker_selected.svg', prefix: 'map-matching-selected' }
            ]

            // making all necessary svg markers by fetching and replacing the sequence
            const values = []
            for (let i = 1; i <= 16; i++) {
              values.push(i)
            }
            values.push('A')
            values.push('B')
            markers.forEach((marker) => {
              svgPromises.push(new Promise((svgResolve) => {
                DM.get(marker.file, 'text').then((text) => {
                  const promises = []
                  values.forEach((value) => {
                    promises.push(new Promise((resolve) => {
                      const image = new Image(35, 35);
                      image.onload = () => {
                        if (this.map) {
                          this.map.addImage(`${marker.prefix}-${value}`, image);
                          resolve()
                        }
                      };
                      image.src = svg64(text.replace('{v}', value));
                    }))
                  })
                  Promise.all(promises).then(() => {
                    svgResolve()
                  })
                })
              }))
            })
            Promise.all(svgPromises).then(() => {
              this.$store.commit('mapbox/setIconsLoaded', true)
            })
          },
  
          addLayers() {
            const firstSymbolLayer = this.getFirstSymbolLayer()
            if (firstSymbolLayer === null) {
              throw new Error('No symbol layers, map is probably corrupted')
            }

            this.addLayerHandlers.forEach((handler) => {
              handler()
            })
          },
  
          updateCursor() {
            let cursor = this.defaultCursor
            if (this.$store.state.mapbox.cursorPoint) {
              // getting features under the cursor and its padding
              const features = this.map.queryRenderedFeatures([
                [this.$store.state.mapbox.cursorPoint.x - this.mousePadding, this.$store.state.mapbox.cursorPoint.y - this.mousePadding],
                [this.$store.state.mapbox.cursorPoint.x + this.mousePadding, this.$store.state.mapbox.cursorPoint.y + this.mousePadding]
              ])

              // collecting all horizon features
              const horizonFeatures = features.filter((feature) => {
                return Object.values(this.$store.state.mapbox.layers).includes(feature.layer.id)
              })

              // seting the cursor to given state from the feature
              // properties if horizon feature is present
              if (horizonFeatures.length > 0) {
                if (horizonFeatures[0].properties.cursor !== undefined) {
                  cursor = horizonFeatures[0].properties.cursor
                }
              }
            }

            // updating cursor style
            this.map.getCanvas().style.cursor = cursor
          },

          // getting ISO date without milliseconds and timezone
          currentTime() {
            const now = new Date()
            return now.toISOString().split('.')[0]
          },

          // fitting to given geometry considering interface state
          fitBounds(geojson) {
            const bbox = turf.bbox(geojson)
            const params = {
              padding: {
                top: 25,
                right: 25,
                bottom: 25,
                left: 25
              }
            }
            if (this.$store.state.interface.expanded) {
              params.padding.left = params.padding.left + this.$store.state.interface.width
            }
            this.map.fitBounds([[bbox[0], bbox[1]], [bbox[2], bbox[3]]], params)
          },

          lineGradientAnimation(mixinKey, layerID) {
            const lineGradientBase = ['interpolate', ['linear'], ['line-progress']]

            // mapbox interpolate values
            let gradient = []

            // calculating gradient stops
            const beg = this[mixinKey].line.progress
            let mid = beg + this[mixinKey].line.gap
            if (mid > 1) {
              mid = mid - 1
            }
            let fin = mid + this[mixinKey].line.gap
            if (fin > 1) {
              fin = fin - 1
            }

            const breaks = {}
            breaks[beg.toFixed(2)] = this[mixinKey].line.baseColor
            breaks[mid.toFixed(2)] = this[mixinKey].line.activeColor
            breaks[fin.toFixed(2)] = this[mixinKey].line.baseColor

            Object.keys(breaks).map((key) => {
              return parseFloat(key)
            }).sort().forEach((key) => {
              gradient = [...gradient, ...[key, breaks[key.toFixed(2)]]]
            })

            if (beg < mid && mid < fin) {
              if (!gradient.includes(0)) {
                gradient = [...[0, this[mixinKey].line.baseColor], ...gradient]
              }
              if (!gradient.includes(1)) {
                gradient = [...gradient, ...[1, this[mixinKey].line.baseColor]]
              }
            } else {
              if (!gradient.includes(0)) {
                gradient = [...[0, this[mixinKey].line.baseColor], ...gradient]
              }
              if (!gradient.includes(1)) {
                gradient = [...gradient, ...[1, this[mixinKey].line.baseColor]]
              }
            }

            // updating line gradient without data recalculation
            if (this.map.getLayer(layerID)) {
              this.map.setPaintProperty(layerID, 'line-gradient', [...lineGradientBase, ...gradient])
            }

            // calculating blobs next position
            this[mixinKey].line.progress += this[mixinKey].line.step
            if (this[mixinKey].line.progress > 1) {
              this[mixinKey].line.progress = this[mixinKey].line.progress - 1
            }
          }
        },
  
        beforeDestroy() {
          this.$root.$emit('eventBus-disable', this.eventBusHandlers)
          this.$root.$emit('ssc-removeStyleSheet', params.path)
        },
  
        watch: {
          defaultCursor: function (n) {
            this.map.getCanvas().style.cursor = n;
          }
        }
      })
    })
  })
}