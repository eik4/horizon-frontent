export default {
  data() {
    return {
      mapMatching: {
        // does any gps point currently being dragged?
        draggingGpsPoint: false,

        // gps point currently is being dragged
        currentGpsPointIndex: null,

        // line animation params
        line: {

          // id for animation
          timerID: null,

          // blob position (0 - 1)
          progress: 0,
          step: 0.02,

          // blob length
          gap: 0.05,

          // line color
          baseColor: '#999',

          // blob color
          activeColor: '#111'
        }
      }
    }
  },

  beforeMount() {
    this.eventBusHandlers = {
      ...this.eventBusHandlers,
      'mapbox-updateGpsPoints': this.updateGpsPoints,
      'mapbox-updateMapMatchingLine': this.updateMapMatchingLine,
      'mapbox-emptyMapMatchingGeometries': this.emptyMapMatchingGeometries
    }

    this.addLayerHandlers.push(() => {
      // initializing with empty geojson
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      const dataSource = {
        type: 'geojson',
        data: geojson
      }

      // line with animated gradient
      this.map.addSource(this.$store.state.mapbox.layers.mapMatchingLine, { ...dataSource, lineMetrics: true })
      this.map.addLayer({
        id: this.$store.state.mapbox.layers.mapMatchingLine,
        type: 'line',
        source: this.$store.state.mapbox.layers.mapMatchingLine,
        paint: {
          'line-color': this.mapMatching.line.baseColor,
          'line-width': 8,
          'line-opacity': 0.5,
          'line-gradient': [
            'interpolate',
            ['linear'],
            ['line-progress'],
            0,
            '#999',
            0.05,
            '#111',
            0.1,
            '#999',
            1,
            '#999'
          ]
        },
        layout: {
          'line-cap': 'round',
          'line-join': 'round'
        }
      })

      // line animation
      this.mapMatching.line.timerID = setInterval(() => {
        this.lineGradientAnimation(
          'mapMatching',
          this.$store.state.mapbox.layers.mapMatchingLine
        )
      }, 50)

      // points with letters
      this.map.addSource(this.$store.state.mapbox.layers.mapMatchingPoints, dataSource)
      this.map.addLayer({
        id: this.$store.state.mapbox.layers.mapMatchingPoints,
        type: 'symbol',
        source: this.$store.state.mapbox.layers.mapMatchingPoints,
        layout: {
          'icon-image': ['get', 'icon'],
          'icon-allow-overlap': true
        }
      })
    })
  },

  methods: {
    // adding new gps point on clicked location on the map
    addNewGpsPoint(e) {
      if (this.$store.state.mapMatching.points.length < this.$store.state.mapMatching.maxPoints) {
        this.$store.commit('mapMatching/addPoint', {
          lonLat: [e.lngLat.lng, e.lngLat.lat],
          tm: this.currentTime()
        })
        this.updateGpsPoints()
        this.updateCursor()
      }
    },

    // gps point drag have been started
    dragstartGpsPoint(e) {

      // getting gps point under the cursor and its padding
      const features = this.map.queryRenderedFeatures([
        [e.point.x - this.mousePadding, e.point.y - this.mousePadding],
        [e.point.x + this.mousePadding, e.point.y + this.mousePadding]
      ])
      const mapMathingFeatures = features.filter((feature) => {
        return [
          this.$store.state.mapbox.layers.mapMatchingPoints,
          this.$store.state.mapbox.layers.mapMatchingLine
        ].includes(feature.layer.id)
      })

      // disable map panning and rotating
      // if gps point is present
      // in order to drag the feature
      if (mapMathingFeatures.length > 0) {
        this.map.dragPan.disable()
        this.map.dragRotate.disable()
        this.mapMatching.draggingGpsPoint = true
        this.mapMatching.currentGpsPointIndex = mapMathingFeatures[0].properties.index
      }
    },

    // gps point dragging
    dragGpsPoint(e) {
      if (this.mapMatching.draggingGpsPoint) {
        this.$store.commit('mapMatching/updatePoint', {
          index: this.mapMatching.currentGpsPointIndex,
          coordinateIndex: 0,
          value: e.lngLat.lng
        })
        this.$store.commit('mapMatching/updatePoint', {
          index: this.mapMatching.currentGpsPointIndex,
          coordinateIndex: 1,
          value: e.lngLat.lat
        })

        // updating values on the interface correspondingly to current position
        this.$root.$emit('map-matching-updateInputByIndex', this.mapMatching.currentGpsPointIndex, e.lngLat)
        this.$root.$emit('mapbox-updateGpsPoints')
      }
    },

    // gps point drag have been finished
    dragendGpsPoint() {
      this.map.dragPan.enable()
      this.map.dragRotate.enable()
      this.mapMatching.draggingGpsPoint = false
      this.mapMatching.currentGpsPointIndex = null
    },

    // redrawing gps point
    updateGpsPoints() {
      this.map.getSource(this.$store.state.mapbox.layers.mapMatchingPoints).setData(this.$store.getters['mapMatching/pointsGeojson'])
      this.updateCursor()
    },

    // redrawing map matching line
    updateMapMatchingLine() {
      this.map.getSource(this.$store.state.mapbox.layers.mapMatchingLine).setData(this.$store.state.mapMatching.line)
      this.updateCursor()
      if (this.$store.state.mapMatching.line) {
        if (this.$store.state.mapMatching.line.features.length > 0) {
          this.fitBounds(this.$store.state.mapMatching.line)
        }
      }
    },

    // clearing geometries
    emptyMapMatchingGeometries() {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      this.map.getSource(this.$store.state.mapbox.layers.mapMatchingPoints).setData(geojson)
      this.map.getSource(this.$store.state.mapbox.layers.mapMatchingLine).setData(geojson)
      this.updateCursor()
    }
  },

  beforeDestroy() {
    if (this.mapMatching.line.timerID) {
      clearInterval(this.mapMatching.line.timerID)
      this.mapMatching.line.timerID = null
    }
  }
}