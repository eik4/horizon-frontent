export default {
  data() {
    return {
      isochrones: {
        // does isochrones source point currently being dragged?
        draggingIsochronesPoint: false
      }
    }
  },

  beforeMount() {
    this.eventBusHandlers = {
      ...this.eventBusHandlers,
      'mapbox-updateIsochronesSource': this.updateIsochronesSource,
      'mapbox-updateIsochronesPoints': this.updateIsochronesPoints,
      'mapbox-updateIsochronesPolygons': this.updateIsochronesPolygons,
      'mapbox-emptyIsochronesGeometries': this.emptyIsochronesGeometries,
      'mapbox-emptyIsochronesPoints': this.emptyIsochronesPoints,
      'mapbox-emptyIsochronesPolygons': this.emptyIsochronesPolygons
    }

    this.addLayerHandlers.push(() => {
      // initializing with empty geojson
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      const dataSource = {
        type: 'geojson',
        data: geojson
      }

      // polygons
      // fill and opacity defines on feature properties
      this.map.addSource(this.$store.state.mapbox.layers.isochronesPolygons, dataSource)
      this.map.addLayer({
        id: this.$store.state.mapbox.layers.isochronesPolygons,
        type: 'fill',
        source: this.$store.state.mapbox.layers.isochronesPolygons,
        paint: {
          'fill-color': ['get', 'color'],
          'fill-opacity': ['get', 'opacity']
        }
      })

      // points
      // color, opacity, and raduis defines on feature properties
      this.map.addSource(this.$store.state.mapbox.layers.isochronesPoints, dataSource)
      this.map.addLayer({
        id: this.$store.state.mapbox.layers.isochronesPoints,
        type: 'circle',
        source: this.$store.state.mapbox.layers.isochronesPoints,
        paint: {
          'circle-color': ['get', 'color'],
          'circle-opacity': ['get', 'opacity'],
          'circle-radius': ['get', 'radius']
        }
      })

      // source point
      // color, opacity, and raduis defines on feature properties
      this.map.addSource(this.$store.state.mapbox.layers.isochronesSource, dataSource)
      this.map.addLayer({
        id: this.$store.state.mapbox.layers.isochronesSource,
        type: 'circle',
        source: this.$store.state.mapbox.layers.isochronesSource,
        paint: {
          'circle-color': ['get', 'color'],
          'circle-opacity': ['get', 'opacity'],
          'circle-radius': ['get', 'radius']
        }
      })

    })
  },

  methods: {
    // setting source to clicked location on the map
    addIsochronesSource(e) {
      this.$store.commit('isochrones/setSource', [e.lngLat.lng, e.lngLat.lat])
      this.updateIsochronesSource()
      this.updateCursor()
    },

    // isochrones source point drag have been started
    dragstartIsochronesSource(e) {

      // getting isochrone source point under the cursor and its padding
      const features = this.map.queryRenderedFeatures([
        [e.point.x - this.mousePadding, e.point.y - this.mousePadding],
        [e.point.x + this.mousePadding, e.point.y + this.mousePadding]
      ])
      const isocronesFeatures = features.filter((feature) => {
        return feature.layer.id === this.$store.state.mapbox.layers.isochronesSource
      })

      // disable map panning and rotating
      // if isochrone source point is present
      // in order to drag the feature
      if (isocronesFeatures.length > 0) {
        this.map.dragPan.disable()
        this.map.dragRotate.disable()
        this.isochrones.draggingIsochronesPoint = true
      }
    },

    // isochrones source point dragging
    dragIsochronesSource(e) {
      if (this.isochrones.draggingIsochronesPoint) {
        this.$store.commit('isochrones/updateSource', {
          coordinateIndex: 0,
          value: e.lngLat.lng
        })
        this.$store.commit('isochrones/updateSource', {
          coordinateIndex: 1,
          value: e.lngLat.lat
        })
        this.$root.$emit('mapbox-updateIsochronesSource')

        // updating values on the interface correspondingly to current position
        this.$root.$emit('isochrones-updateInputValues')
      }
    },

    // isochrones source point drag have been finished
    dragendIsochronesSource() {
      this.map.dragPan.enable()
      this.map.dragRotate.enable()
      this.isochrones.draggingIsochronesPoint = false
    },

    // redrawing isochrone source point
    updateIsochronesSource() {
      this.map.getSource(this.$store.state.mapbox.layers.isochronesSource).setData(this.$store.getters['isochrones/sourceGeojson'])
      this.updateCursor()
    },

    // redrawing isochrone points if necessary
    updateIsochronesPoints() {
      if (this.$store.state.isochrones.result.points) {
        const geojson = this.$store.getters['isochrones/points']
        this.map.getSource(this.$store.state.mapbox.layers.isochronesPoints).setData(geojson)
        this.updateCursor()
        if (geojson.features.length > 0) {
          this.fitBounds(geojson)
        }
      }
    },

    // redrawing isochrone polygons if necessary
    updateIsochronesPolygons() {
      if (this.$store.state.isochrones.result.polygons) {
        const geojson = this.$store.getters['isochrones/polygons']
        this.map.getSource(this.$store.state.mapbox.layers.isochronesPolygons).setData(geojson)
        this.updateCursor()
        if (geojson.features.length > 0) {
          this.fitBounds(geojson)
        }
      }
    },

    // clearing geometries
    emptyIsochronesGeometries() {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      this.map.getSource(this.$store.state.mapbox.layers.isochronesSource).setData(geojson)
      this.map.getSource(this.$store.state.mapbox.layers.isochronesPoints).setData(geojson)
      this.map.getSource(this.$store.state.mapbox.layers.isochronesPolygons).setData(geojson)
      this.updateCursor()
    },

    emptyIsochronesPoints() {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      this.map.getSource(this.$store.state.mapbox.layers.isochronesPoints).setData(geojson)
      this.updateCursor()
    },

    emptyIsochronesPolygons() {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      this.map.getSource(this.$store.state.mapbox.layers.isochronesPolygons).setData(geojson)
      this.updateCursor()
    }
  },
}