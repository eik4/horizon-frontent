export default {
  data() {
    return {
      shortestPath: {
        // does any gps point currently being dragged?
        draggingSPPoint: false,

        // gps point currently is being dragged
        currentSPPointIndex: null,
        // line animation params
        line: {

          // id for animation
          timerID: null,

          // blob position (0 - 1)
          progress: 0,
          step: 0.02,

          // blob length
          gap: 0.05,

          // line color
          baseColor: '#999',

          // blob color
          activeColor: '#111'
        }
      }
    }
  },

  beforeMount() {
    this.eventBusHandlers = {
      ...this.eventBusHandlers,
      'mapbox-updateSPPoints': this.updateSPPoints,
      'mapbox-updateShortestPathLine': this.updateShortestPathLine,
      'mapbox-emptyShortestPathGeometries': this.emptyShortestPathGeometries
    }

    this.addLayerHandlers.push(() => {
      // initializing with empty geojson
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      const dataSource = {
        type: 'geojson',
        data: geojson
      }

      // line with animated gradient
      this.map.addSource(this.$store.state.mapbox.layers.shortestPathLine, { ...dataSource, lineMetrics: true })
      this.map.addLayer({
        id: this.$store.state.mapbox.layers.shortestPathLine,
        type: 'line',
        source: this.$store.state.mapbox.layers.shortestPathLine,
        paint: {
          'line-color': this.shortestPath.line.baseColor,
          'line-width': 8,
          'line-opacity': 0.5,
          'line-gradient': [
            'interpolate',
            ['linear'],
            ['line-progress'],
            0,
            '#999',
            0.05,
            '#111',
            0.1,
            '#999',
            1,
            '#999'
          ]
        },
        layout: {
          'line-cap': 'round',
          'line-join': 'round'
        }
      })

      // line animation
      this.shortestPath.line.timerID = setInterval(() => {
        this.lineGradientAnimation(
          'shortestPath',
          this.$store.state.mapbox.layers.shortestPathLine
        )
      }, 50)

      // points with numbers
      this.map.addSource(this.$store.state.mapbox.layers.shortestPathPoints, dataSource)
      this.map.addLayer({
        id: this.$store.state.mapbox.layers.shortestPathPoints,
        type: 'symbol',
        source: this.$store.state.mapbox.layers.shortestPathPoints,
        layout: {
          'icon-image': ['get', 'icon'],
          'icon-allow-overlap': true
        }
      })
    })
  },

  methods: {
    // adding new gps point on clicked location on the map
    addNewSPPoint(e) {
      if (this.$store.state.shortestPath.points.length < this.$store.state.shortestPath.maxPoints) {
        this.$store.commit('shortestPath/addPoint', {
          lonLat: [e.lngLat.lng, e.lngLat.lat]
        })
        this.updateSPPoints()
        this.updateCursor()
      }
    },

    // gps point drag have been started
    dragstartSPPoint(e) {

      // getting gps point under the cursor and its padding
      const features = this.map.queryRenderedFeatures([
        [e.point.x - this.mousePadding, e.point.y - this.mousePadding],
        [e.point.x + this.mousePadding, e.point.y + this.mousePadding]
      ])
      const mapMathingFeatures = features.filter((feature) => {
        return [
          this.$store.state.mapbox.layers.shortestPathPoints,
          this.$store.state.mapbox.layers.shortestPathLine
        ].includes(feature.layer.id)
      })

      // disable map panning and rotating
      // if gps point is present
      // in order to drag the feature
      if (mapMathingFeatures.length > 0) {
        this.map.dragPan.disable()
        this.map.dragRotate.disable()
        this.shortestPath.draggingSPPoint = true
        this.shortestPath.currentSPPointIndex = mapMathingFeatures[0].properties.index
      }
    },

    // gps point dragging
    dragSPPoint(e) {
      if (this.shortestPath.draggingSPPoint) {
        this.$store.commit('shortestPath/updatePoint', {
          index: this.shortestPath.currentSPPointIndex,
          coordinateIndex: 0,
          value: e.lngLat.lng
        })
        this.$store.commit('shortestPath/updatePoint', {
          index: this.shortestPath.currentSPPointIndex,
          coordinateIndex: 1,
          value: e.lngLat.lat
        })

        // updating values on the interface correspondingly to current position
        this.$root.$emit('shortest-path-updateInputByIndex', this.shortestPath.currentSPPointIndex, e.lngLat)
        this.$root.$emit('mapbox-updateSPPoints')
      }
    },

    // gps point drag have been finished
    dragendSPPoint() {
      this.map.dragPan.enable()
      this.map.dragRotate.enable()
      this.shortestPath.draggingSPPoint = false
      this.shortestPath.currentSPPointIndex = null
    },

    // redrawing gps point
    updateSPPoints() {
      this.map.getSource(this.$store.state.mapbox.layers.shortestPathPoints).setData(this.$store.getters['shortestPath/pointsGeojson'])
      this.updateCursor()
    },

    // redrawing map matching line
    updateShortestPathLine() {
      this.map.getSource(this.$store.state.mapbox.layers.shortestPathLine).setData(this.$store.state.shortestPath.line)
      this.updateCursor()
      if (this.$store.state.shortestPath.line) {
        if (this.$store.state.shortestPath.line.features.length > 0) {
          this.fitBounds(this.$store.state.shortestPath.line)
        }
      }
    },

    // clearing geometries
    emptyShortestPathGeometries() {
      const geojson = {
        type: 'FeatureCollection',
        features: []
      }
      this.map.getSource(this.$store.state.mapbox.layers.shortestPathPoints).setData(geojson)
      this.map.getSource(this.$store.state.mapbox.layers.shortestPathLine).setData(geojson)
      this.updateCursor()
    }
  },

  beforeDestroy() {
    if (this.shortestPath.line.timerID) {
      clearInterval(this.shortestPath.line.timerID)
      this.shortestPath.line.timerID = null
    }
  }
}